// require 'assert'

function divmod (a, b) {
  let remainder = a
  let quotient = 0
  if (a >= b) {
    remainder = a % b
    quotient = (a - remainder) / b
  }
  return [quotient, remainder]
}

const BASE45_CHARSET = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:'

export default function decode (str) {
  const output = []
  const buf = []

  for (var i = 0, length = str.length; i < length; i++) {
    const j = BASE45_CHARSET.indexOf(str[i])
    if (j < 0) { throw new Error('Base45 decode: unknown character') }
    buf.push(j)
  }

  for (var i = 0, length = buf.length; i < length; i += 3) {
    const x = buf[i] + buf[i + 1] * 45
    if (length - i >= 3) {
      const [d, c] = divmod(x + buf[i + 2] * 45 * 45, 256)
      output.push(d)
      output.push(c)
    } else {
      output.push(x)
    }
  }
  return Buffer.from(output)
}
