import '../third-party/nacl.min.js'

const template = `
	<div class="qr-code-scanner--wrapper">
		<div class="qr-code-scanner--video-wrapper">
		  <video class="qr-code-scanner--video"></video>
		  <div class="qr-code-scanner--fullscreen-button">&#xf065;</div>
		</div>
		<div class="qr-code-scanner--error errornotice" hidden></div>
		<div class="qr-code-scanner--popup closed">
			<div class="qr-code-scanner--results"></div>
			<button class="qr-code-scanner--close-popup-button">Close</button>
		</div>
	</div>
	<template class="qr-code-scanner--item-template">
	  <div class="qr-code-scanner--item">
		<span class="qr-code-scanner--item-label">{label}&nbsp;:&nbsp;</span>
		<span class="qr-code-scanner--item-value">{value}</span>
	  </div>
	</template>
`

const notSupportedTemplate = '<p>QR Code scanner is not supported on your platform. Please update your browser.</p>'

function decodeMimeLike (value) {
  const chunks = value.split('\n')
  const data = {}
  let k = null
  let v = null

  for (let i = 0; i < chunks.length; i++) {
    const line = chunks[i]
    if (line.startsWith(' ')) {
      if (k !== null) {
        v += '\n' + line.slice(1)
      }
    } else {
      if (k !== null) {
        data[k] = v
        k = null
        v = null
      }
      if (line.indexOf(': ') != -1) {
        const parts = line.split(': ', 2)
        k = parts[0]
        v = parts[1]
      }
    }
  }

  if (k !== null) {
    data[k] = v
  }

  return data
}

function divmod (a, b) {
  let remainder = a
  let quotient = 0
  if (a >= b) {
    remainder = a % b
    quotient = (a - remainder) / b
  }
  return [quotient, remainder]
}

const BASE45_CHARSET = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:'

function decodeBase45 (str) {
  const output = []
  const buf = []

  for (var i = 0, length = str.length; i < length; i++) {
    const j = BASE45_CHARSET.indexOf(str[i])
    if (j < 0) { throw new Error('Base45 decode: unknown character') }
    buf.push(j)
  }

  for (var i = 0, length = buf.length; i < length; i += 3) {
    const x = buf[i] + buf[i + 1] * 45
    if (length - i >= 3) {
      const [d, c] = divmod(x + buf[i + 2] * 45 * 45, 256)
      output.push(d)
      output.push(c)
    } else {
      output.push(x)
    }
  }
  return new Uint8Array(output)
}

function toggleFullScreen (element) {
  if (document.fullscreenElement) {
    if (document.exitFullscreen) {
      document.exitFullscreen()
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen()
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen()
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen()
    }
  } else {
    if (!document.mozFullScreen && !document.webkitFullScreen) {
      if (element.requestFullscreen) {
        element.requestFullscreen()
      } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen()
      } else if (element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen()
      } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen()
      }
    }
  }
}

class QrCodeScanner extends window.HTMLElement {
  #error
  #errorTimeout
  #resultItemTemplate
  #popup
  #results
  #video

  constructor () {
    super()
    this.innerHTML = template

    this.#error = this.querySelector('.qr-code-scanner--error')
    this.#resultItemTemplate = this.querySelector('.qr-code-scanner--item-template')
    this.#popup = this.querySelector('.qr-code-scanner--popup')
    this.#results = this.querySelector('.qr-code-scanner--results')
    this.#video = this.querySelector('.qr-code-scanner--video')

    const closePopupButton = this.querySelector('.qr-code-scanner--close-popup-button')
    closePopupButton.addEventListener('click', () => {
      this.#popup.classList.add('closed')
    })

    const fullScreenButton = this.querySelector('.qr-code-scanner--fullscreen-button')
    const wrapper = this.querySelector('.qr-code-scanner--wrapper')
    fullScreenButton.addEventListener('click', () => {
      toggleFullScreen(wrapper)
    })
  }

  connectedCallback () {
    this.#startScan()
  }

  async #startScan () {
    const codeReader = new ZXingBrowser.BrowserQRCodeReader()
    const controls = await codeReader.decodeFromVideoDevice(undefined, this.#video, (result, error, controls) => {
      if (result) {
        try {
          this.#showResult(result.text)
        } catch (error) {
          console.log(error)
        }
      }
    })
  }

  get #verifyKey () {
    const hexKey = this.getAttribute('verify-key')
    return new Uint8Array(hexKey.match(/[\da-f]{2}/gi).map(h => parseInt(h, 16)))
  }

  #supported () {
    return !!navigator.mediaDevices
  }

  #showResult (qrCodeContent) {
    this.#error.hidden = true
    this.#popup.classList.add('closed')

    let signed
    try {
      signed = decodeBase45(qrCodeContent)
    } catch (error) {
      this.#showError('Invalid QR code content')
      return
    }

    const opened = nacl.sign.open(signed, this.#verifyKey)
    if (opened == null) {
      this.#showError('Signature validation failed')
      return
    }

    const decoder = new TextDecoder('utf-8')
    const decoded = decoder.decode(opened)
    const data = decodeMimeLike(decoded)

    this.#results.innerHTML = ''
    for (const [key, value] of Object.entries(data)) {
      const resultItem = this.#resultItemTemplate.cloneNode(true)
      resultItem.innerHTML = resultItem.innerHTML.replace('{label}', key).replace('{value}', value)
      this.#results.append(resultItem.content)
    }
    this.#popup.classList.remove('closed')
  }

  #showError (message) {
    this.#error.hidden = false
    this.#error.innerText = message
    if (this.#errorTimeout) {
      clearTimeout(this.#errorTimeout)
    }

    this.#errorTimeout = setTimeout(() => {
      this.#errorTimeout = undefined
      this.#error.hidden = true
    }, 4000)
  }
}

window.customElements.define('qr-code-scanner', QrCodeScanner)
